﻿using System.Collections.Generic;
using System.Linq;
using FullSerializer;

namespace Helpers.Serializers.Json
{
	public sealed class JsonSerializer
    {
        private static readonly fsSerializer serializer = new fsSerializer();

        public static string Serialize<T>(T value, bool compressed = true)
        {
	        serializer.TrySerialize(typeof(T), value, out var data).AssertSuccessWithoutWarnings();
			StripDeserializationMetadata(ref data);
			return compressed ? fsJsonPrinter.CompressedJson(data) : fsJsonPrinter.PrettyJson(data);
		}

        public static T Deserialize<T>(string serializedState)
        {
            var data = fsJsonParser.Parse(serializedState);
            object deserialized = null;
            serializer.TryDeserialize(data, typeof(T), ref deserialized).AssertSuccessWithoutWarnings();
            return (T)deserialized;
        }

        private static void StripDefaultData(Dictionary<string, fsData> jsonDictionary)
        {
	        var index = jsonDictionary.Count - 1;
			
	        for (int j = index; j >= 0; j--)
	        {
		        var value = jsonDictionary.ElementAt(j).Value;
		        var isNull = value.IsNull;
		        var isFalseBool = value.IsBool && value.AsBool == false;
		        var isZeroInt = value.IsInt64 && value.AsInt64 == 0;
		        var isZeroOrNullCollection = value.IsList && (value.AsList == null || value.AsList.Count == 0);
		        var isNullOrEmptyString = value.IsString && string.IsNullOrEmpty(value.AsString);				

		        if (isNull || isFalseBool || isNullOrEmptyString || isZeroInt || isZeroOrNullCollection)
		        {
			        jsonDictionary.Remove(jsonDictionary.ElementAt(j).Key);
		        }
	        }
        }
        
		private static void StripDeserializationMetadata(ref fsData data)
		{
			if (data.IsList)
			{
				var list = data.AsList;
				
				for (int i = list.Count -1; i >= 0; i--)
				{
					if(list[i].IsDictionary)
					{
						var itemAsDictionary = list[i].AsDictionary;
						StripDefaultData(itemAsDictionary);
					}
				}
			}
			
			if (data.IsDictionary && data.AsDictionary.ContainsKey("$content"))
				data = data.AsDictionary["$content"];
			if (!data.IsDictionary)
				return;
			var asDictionary = data.AsDictionary;
			asDictionary.Remove("$ref");
			asDictionary.Remove("$id");
			asDictionary.Remove("$version");
			
			for (var i = 0; i < asDictionary.Count; i++)
			{
				var pair = asDictionary.ElementAt(i);
				var child = pair.Value;
				StripDeserializationMetadata(ref child);
				asDictionary[pair.Key] = child;
				StripDefaultData(asDictionary);
			}
		}
    }
}
