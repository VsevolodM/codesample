﻿using Core.Types;
using System;
using System.Collections;
using UnityEngine;

namespace Helpers
{
    public class CoroutineHelper : CustomMonobehaviour
    {
        private WaitForEndOfFrame _waitForEndOfFrame;
        
        private void Awake()
        {
            gameObject.hideFlags = HideFlags.HideInHierarchy;
            _waitForEndOfFrame = new WaitForEndOfFrame();
        }

        public Coroutine ExecuteWithDelay(float delay, Action action)
        {
            return StartCoroutine(ExecuteWithDelayCoroutine(delay, action));
        }

        public Coroutine ExecuteWithFrameDelay(Action action)
        {
            return ExecuteWithFramesDelay(1, action);
        }

        public Coroutine ExecuteWithFramesDelay(int framesToWait, Action action)
        {
            return StartCoroutine(ExecuteWithFramesDelayCoroutine(framesToWait, action));
        }

        public IEnumerator WaitForFrames(int framesToWait)
        {
            for (int i = 0; i < framesToWait; i++)
            {
                yield return new WaitUntil(() => Time.timeScale >= 1f);
                yield return _waitForEndOfFrame;
            }
        }

        private IEnumerator ExecuteWithFramesDelayCoroutine(int framesToWait, Action action)
        {
            yield return WaitForFrames(framesToWait);
            action?.Invoke();
        }

        private IEnumerator ExecuteWithDelayCoroutine(float delay, Action action)
        {
            yield return new WaitForSeconds(delay);
            action?.Invoke();
        }
    }
}
