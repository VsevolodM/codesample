﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Extensions
{
    public static class CollectionsExtensions
    {
        public static T GetRandom<T>(this IEnumerable<T> enumerable)
        {
            var index = enumerable.GetRandomIndex();
            return enumerable.ElementAt(index);
        }

        public static int GetRandomIndex<T>(this IEnumerable<T> enumerable)
        {
            return Random.Range(0, enumerable.Count());
        }
    }
}