﻿using System;
using UnityEngine;

namespace Extensions
{
    public static class GameObjectExtensions
    {
        public static GameObject CreateNewGameObject(string name, params Type[] components)
        {
            return CreateNewGameObject(name, null, components);
        }
        
        public static GameObject CreateNewGameObject(string name, Transform parent, params Type[] components)
        {
            var gameObject = new GameObject(name, components);
            gameObject.transform.SetParent(parent);
            gameObject.transform.localPosition = Vector3.zero;
            return gameObject;
        }
        
        public static T AddComponentIfNotPresent<T>(this GameObject gameObject) where T : Component
        {
            var component = gameObject.GetComponent<T>();
            if (component != null) return component;
            return gameObject.AddComponent<T>();
        }
        
        public static void SafeStopCoroutine(this MonoBehaviour monoBehaviour, Coroutine routine)
        {
            if (routine != null)
            {
                monoBehaviour.StopCoroutine(routine);
            }
        }

        public static void RemoveCloneFromName(this GameObject gameObject)
        {
            gameObject.name = gameObject.name.Replace("(Clone)", string.Empty);
        }
    }
}
