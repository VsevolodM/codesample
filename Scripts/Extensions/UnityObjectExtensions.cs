﻿using UnityEngine;

namespace Extensions
{
    public static class UnityObjectExtensions
    {
        public static T InstantiateFromResources<T>(string path) where T: Behaviour
        {
            return InstantiateFromResources<T>(path, Vector3.zero, Quaternion.identity, null);
        }
        
        public static T InstantiateFromResources<T>(string path, Transform parent) where T: Behaviour
        {
            return InstantiateFromResources<T>(path, Vector3.zero, Quaternion.identity, parent);
        }

        public static T InstantiateFromResources<T>(string path, Vector3 position, Transform parent) where T: Behaviour
        {
            return InstantiateFromResources<T>(path, position, Quaternion.identity, parent);
        }
        
        public static T InstantiateFromResources<T>(string path, Vector3 position, Quaternion rotation, Transform parent) where T: Behaviour
        {
            var prefab = Resources.Load<T>(path);

            if (prefab == null)
            {
                Debug.LogError($"Prefab does not exist on path: {path}");
            }

            var spawnedPrefab = Object.Instantiate(prefab, position, rotation, parent);
            spawnedPrefab.gameObject.RemoveCloneFromName();
            return spawnedPrefab;
        }
    }
}
