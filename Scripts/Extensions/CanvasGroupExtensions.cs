﻿using UnityEngine;

namespace Extensions
{
    public static class CanvasGroupExtensions
    {
        public static void Enable(this CanvasGroup canvasGroup, bool value)
        {
            canvasGroup.alpha = value ? 1f : 0f;
            canvasGroup.blocksRaycasts = value;
            canvasGroup.interactable = value;
        }
    }
}
