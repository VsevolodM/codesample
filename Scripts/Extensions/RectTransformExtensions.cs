﻿using Core;
using UnityEngine;

namespace Extensions
{
    public static class RectTransformExtensions
    {
        public static void SetSizeData(this RectTransform rectTransform, SizeData sizeData)
        {
            var parent = rectTransform.parent;

            if (parent == null) return;

            var parentSize = parent.GetComponent<RectTransform>().GetSize();
            var newSize = new Vector2(parentSize.x * sizeData.ParentRelativeScale.x, parentSize.y * sizeData.ParentRelativeScale.y);
            rectTransform.SetSize(newSize);
            rectTransform.anchoredPosition = new Vector2(parentSize.x * sizeData.NormalizedPositionInParent.x, parentSize.y * sizeData.NormalizedPositionInParent.y);
            rectTransform.eulerAngles = sizeData.Rotation;
        }
        
        public static void FillParent(this RectTransform rectTransform)
        {
            rectTransform.pivot = Vector2.one * 0.5f;
            rectTransform.anchorMin = Vector2.zero;
            rectTransform.anchorMax = Vector2.one;
            rectTransform.sizeDelta = Vector2.zero;
            rectTransform.anchoredPosition = Vector3.zero;
            rectTransform.localPosition = new Vector3(rectTransform.localPosition.x, rectTransform.localPosition.y, 0f);
            rectTransform.transform.localScale = Vector3.one;
        }

        public static void SetDefaultScale(this RectTransform trans)
        {
            trans.localScale = Vector3.one;
        }

        public static void SetPivotAndAnchors(this RectTransform trans, Vector2 vec)
        {
            trans.pivot = vec;
            trans.anchorMin = vec;
            trans.anchorMax = vec;
        }

        public static void SetPivotAndAnchors(this RectTransform trans, Vector2 pivot, Vector2 anchorMin, Vector2 anchorMax)
        {
            trans.pivot = pivot;
            trans.anchorMin = anchorMin;
            trans.anchorMax = anchorMax;
        }

        public static Vector2 GetSize(this RectTransform trans)
        {
            return trans.rect.size;
        }

        public static void SetSize(this RectTransform trans, Vector2 newSize)
        {
            Vector2 oldSize = trans.rect.size;
            Vector2 deltaSize = newSize - oldSize;
            trans.offsetMin = trans.offsetMin - new Vector2(deltaSize.x * trans.pivot.x, deltaSize.y * trans.pivot.y);
            trans.offsetMax = trans.offsetMax + new Vector2(deltaSize.x * (1f - trans.pivot.x), deltaSize.y * (1f - trans.pivot.y));
        }
    }
}