﻿using System;
using DG.Tweening;
using UnityEngine;

namespace Extensions
{
    public static class DoTweenExtensions
    {
        public static Tween DoTweenWithUpdateAndComplete(double variable, double targetValue, float speedOrDuration, Action<double> updateAndCompletedAction, bool speedBased = false)
        {
            return DOTween.To(() => variable, x => variable = x, targetValue, speedOrDuration)
                .OnUpdate(() => updateAndCompletedAction?.Invoke(variable))
                .SetSpeedBased(speedBased)
                .OnComplete(() => updateAndCompletedAction?.Invoke(variable));
        }
        
        public static Tween DoTweenWithUpdateAndComplete(float variable, float targetValue, float speedOrDuration, Action<float> updateAndCompletedAction, bool speedBased = false)
        {
            return DOTween.To(() => variable, x => variable = x, targetValue, speedOrDuration)
                .OnUpdate(() => updateAndCompletedAction?.Invoke(variable))
                .SetSpeedBased(speedBased)
                .OnComplete(() => updateAndCompletedAction?.Invoke(variable));
        }
        
        public static Tween DoTweenWithUpdateAndComplete(Vector2 variable, Vector2 targetValue, float speedOrDuration, Action<Vector2> updateAndCompletedAction, bool speedBased = false)
        {
            return DOTween.To(() => variable, x => variable = x, targetValue, speedOrDuration)
                .OnUpdate(() => updateAndCompletedAction?.Invoke(variable))
                .SetSpeedBased(speedBased)
                .OnComplete(() => updateAndCompletedAction?.Invoke(variable));
        }
    }
}
