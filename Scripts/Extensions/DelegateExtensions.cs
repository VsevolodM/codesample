﻿using System;

namespace Extensions
{
    public static class DelegateExtensions
    {
        public static void SafeInvoke(this Action action)
        {
            action?.Invoke();
        }

        public static void SafeInvoke<T>(this Action<T> action, T param)
        {
            action?.Invoke(param);
        }

        public static void SafeInvoke<T1, T2>(this Action<T1, T2> action, T1 param1, T2 param2)
        {
            action?.Invoke(param1, param2);
        }

        public static void SafeInvoke<T1, T2, T3>(this Action<T1, T2, T3> action, T1 param1, T2 param2, T3 param3)
        {
            action?.Invoke(param1, param2, param3);
        }

        public static TResult SafeInvoke<TResult>(this Func<TResult> func)
        {
            return func != null ? func() : default(TResult);
        }

        public static TResult SafeInvoke<T, TResult>(this Func<T, TResult> func, T param)
        {
            return func != null ? func(param) : default(TResult);
        }

        public static TResult SafeInvoke<T1, T2, TResult>(this Func<T1, T2, TResult> func, T1 param1, T2 param2)
        {
            return func != null ? func(param1, param2) : default(TResult);
        }

        public static TResult SafeInvoke<T1, T2, T3, TResult>(this Func<T1, T2, T3, TResult> func, T1 param1, T2 param2, T3 param3)
        {
            return func != null ? func(param1, param2, param3) : default(TResult);
        }
    }
}
