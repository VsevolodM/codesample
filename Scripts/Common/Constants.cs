using UnityEngine;

namespace Common
{
    public static class Constants
    {
        public static Color TransparentWhite = new Color(1f,1f,1f,0f);
        public static Color TransparentBlack = new Color(0f, 0f, 0f, 0f);
        public static string PrefabsRootFolder = "Prefabs/UI";
    }
}
