using UI.Services;
using UnityEngine;
using Zenject;

namespace Common
{
    public class EntryPoint : MonoBehaviour
    {
        private NavigationService _navigationService;
        
        [Inject]
        private void Construct(NavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        private void Awake()
        {
            _navigationService.OpenMainMenuScreen();
        }
    }
}
