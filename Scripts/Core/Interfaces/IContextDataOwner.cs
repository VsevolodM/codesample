﻿using Core.Types;

namespace Core.Interfaces
{
    public interface IContextDataOwner<T>
    {
        ContextDataOwner<T> ContextDataOwner { get; }
    }
}
