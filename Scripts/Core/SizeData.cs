﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Core
{
    [Serializable]
    public struct SizeData
    {
        [SerializeField, LabelWidth(200)] private Vector2 parentRelativeScale;
        [SerializeField, LabelWidth(200)] private Vector2 normalizedPositionInParent;
        [SerializeField, LabelWidth(200)] private Vector3 rotation;

        public Vector2 NormalizedPositionInParent => normalizedPositionInParent;
        public Vector2 ParentRelativeScale => parentRelativeScale;
        public Vector3 Rotation => rotation;

        public SizeData(Vector2 parentRelativeScale, Vector2 normalizedPositionInParent, Vector3 rotation)
        {
            this.parentRelativeScale = parentRelativeScale;
            this.normalizedPositionInParent = normalizedPositionInParent;
            this.rotation = rotation;
        }
    }
}

