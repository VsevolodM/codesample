using System;
using DG.Tweening;
using Sirenix.OdinInspector;

namespace Core.Types
{
    public class CustomSerializedMonobehaviour : SerializedMonoBehaviour
    {
        public event Action OnDestroyedEvent;
        
        public bool IsDestroyed { get; private set; }
        
        protected virtual void OnDestroy()
        {
            IsDestroyed = true;
            this.DOKill();
            StopAllCoroutines();
            OnDestroyedEvent?.Invoke();
        }
    }
}
