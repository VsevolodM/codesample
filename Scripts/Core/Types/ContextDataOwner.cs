﻿using System;

namespace Core.Types
{
    public class ContextDataOwner<T>
    {
        public T ContextData { get; private set; }

        private readonly Action _onInitializedAction;
        private readonly Action _onPreUninitializedAction;
        private readonly Action _onDataPreUninitializedAction;

        public ContextDataOwner(Action onInitializedAction, Action onPreUninitializedAction, Action onDataPreUninitializedAction)
        {
            _onInitializedAction = onInitializedAction;
            _onPreUninitializedAction = onPreUninitializedAction;
            _onDataPreUninitializedAction = onDataPreUninitializedAction;
        }

        public void Initialize(T model)
        {
            Uninitialize();
            ContextData = model;
            _onInitializedAction?.Invoke();
        }

        public void Uninitialize()
        {
            if (!Equals(ContextData, default(T)))
            {
                _onDataPreUninitializedAction?.Invoke();
            }
            
            _onPreUninitializedAction?.Invoke();
            ContextData = default;
        }
    }
}
