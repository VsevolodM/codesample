using System;
using DG.Tweening;
using UnityEngine;

namespace Core.Types
{
    public class CustomMonobehaviour : MonoBehaviour
    {
        public event Action OnDestroyedEvent;
        
        public bool IsDestroyed { get; private set; }
        
        protected virtual void OnDestroy()
        {
            IsDestroyed = true;
            this.DOKill();
            StopAllCoroutines();
            OnDestroyedEvent?.Invoke();
        }
    }
}
