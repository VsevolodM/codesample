﻿using UnityEngine;

namespace DataModels
{
    public class RectTransformData
    {
        public Vector2 Pivot { get; private set; }
        public Vector2 AnchorMin { get; private set; }
        public Vector2 AnchorMax { get; private set; }
        public Vector2 AnchoredPosition { get; private set; }
        public Vector2 SizeDelta { get; private set; }

        public void Set(RectTransform rectTransformData)
        {
            Pivot = rectTransformData.pivot;
            AnchorMin = rectTransformData.anchorMin;
            AnchorMax = rectTransformData.anchorMax;
            AnchoredPosition = rectTransformData.anchoredPosition;
            SizeDelta = rectTransformData.sizeDelta;
        }

        public override string ToString()
        {
            return $"{nameof(Pivot)}: {Pivot}, {nameof(AnchorMin)}: {AnchorMin}, {nameof(AnchorMax)}: {AnchorMax}, {nameof(AnchoredPosition)}: {AnchoredPosition}, {nameof(SizeDelta)}: {SizeDelta}";
        }
    }
}
