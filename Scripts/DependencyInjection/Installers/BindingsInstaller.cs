using Helpers;
using Sirenix.OdinInspector;
using UI.Managers;
using UI.Services;
using UnityEngine;
using Zenject;

namespace DependencyInjection.Installers
{
    internal class BindingsInstaller : MonoInstaller
    {
        [SerializeField, ChildGameObjectsOnly] private CoroutineHelper coroutineHelper;
        [SerializeField, ChildGameObjectsOnly] private UiManager uiManager;
        private NavigationService _navigationService;
        
        public override void InstallBindings()
        {
            _navigationService = new NavigationService(uiManager);
            Container.BindInterfacesAndSelfTo<UiManager>().FromInstance(uiManager).AsSingle();
            Container.BindInterfacesAndSelfTo<NavigationService>().FromInstance(_navigationService).AsSingle();
            Container.BindInterfacesAndSelfTo<CoroutineHelper>().FromInstance(coroutineHelper).AsSingle();
        }
    }
}