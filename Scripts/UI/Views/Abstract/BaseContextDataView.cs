﻿using Core.Interfaces;
using Core.Types;

namespace UI.Views.Abstract
{
    public abstract class BaseContextDataView<T> : BaseView, IContextDataOwner<T>
    {
        private ContextDataOwner<T> _contextDataOwner;
        public ContextDataOwner<T> ContextDataOwner => _contextDataOwner ??= new ContextDataOwner<T>(OnInitialized, OnPreUninitialized, OnDataPreUninitialized);

        protected abstract void OnInitialized();
        protected virtual void OnPreUninitialized() {}
        protected virtual void OnDataPreUninitialized() {}

        protected override void OnDestroy()
        {
            base.OnDestroy();
            ContextDataOwner.Uninitialize();
        }
    }
}
