﻿using Core.Types;
using Extensions;
using Sirenix.OdinInspector;
using UI.Managers;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

#pragma warning disable CS0649

namespace UI.Views.Abstract
{
    public abstract class BaseView : CustomSerializedMonobehaviour
    {
        private const string BodyName = "Body";

        public bool IsShown { get; private set; }
        
        [SerializeField] private bool _closeOnEscape;
        [SerializeField, ChildGameObjectsOnly] private RectTransform _body;

        public bool CloseOnEscape
        {
            get => _closeOnEscape;
            protected set => _closeOnEscape = value;
        }

        private RectTransform _rectTransform;

        public RectTransform RectTransform
        {
            get
            {
                if (_rectTransform == null)
                {
                    _rectTransform = GetComponent<RectTransform>();
                }

                return _rectTransform;
            }
        }
        protected RectTransform Body => _body;
        private CanvasGroup _bodyCanvasGroup;

        protected CanvasGroup bodyCanvasGroup
        {
            get
            {
                if (_bodyCanvasGroup == null)
                {
                    _bodyCanvasGroup = _body.GetComponent<CanvasGroup>();
                }

                return _bodyCanvasGroup;
            }
        }
        
        protected UiManager UIManager { get; private set; }
        
        [Inject]
        protected virtual void Construct(UiManager uiManager)
        {
            UIManager = uiManager;
        }

        protected virtual void Awake()
        {
            Assert.IsNotNull(_body);
        }

        public void SetActive(bool value)
        {
            if(value) Show();
            else Hide();
        }
        
        public void Show()
        {
            IsShown = true;
            bodyCanvasGroup.Enable(true);
            OnShown();
        }

        public void Hide()
        {
            IsShown = false;
            bodyCanvasGroup.Enable(false);
            OnHidden();
        }

        public void SetInteractable(bool value)
        {
            bodyCanvasGroup.interactable = value;
        }
        
        public virtual void Close() {}

        protected virtual void OnShown() {}
        protected virtual void OnHidden() {}

        public virtual void OnEscapeKeyUp()
        {
            if (CloseOnEscape)
            {
                Close();
            }
        }

        protected virtual void Reset()
        {
            var className = GetType().Name;
            var bodyName = GetType().Name + BodyName;

            gameObject.name = className;

            GameObject bodyGameObject;

            if (transform.childCount == 0)
            {
                bodyGameObject = new GameObject(bodyName, typeof(CanvasGroup), typeof(RectTransform));
                bodyGameObject.transform.SetParent(transform);
            }
            else
            {
                bodyGameObject = transform.GetChild(0).gameObject;
                bodyGameObject.name = bodyName;
                bodyGameObject.AddComponentIfNotPresent<CanvasGroup>();
            }

            _body = bodyGameObject.GetComponent<RectTransform>();
            _body.FillParent();
        }
    }
}
