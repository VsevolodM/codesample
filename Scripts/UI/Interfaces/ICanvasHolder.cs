﻿using UnityEngine;

namespace UI.Interfaces
{
    public interface ICanvasHolder
    {
        Canvas Canvas { get; }
    }
}
