﻿using Core.Interfaces;
using Helpers;
using UI.Dialogs;
using UI.Dialogs.Abstract;
using UI.Managers.Abstract;
using UI.UIAnimators;
using UnityEngine;
using Object = UnityEngine.Object;

namespace UI.Managers
{
    public class DialogManager : BaseUiManager<BaseDialog>
    {
        private const int SortOrderStep = 2;

        private int _currentSortOrder = 1000;
        private FadeOverlayDialog _overlayDialog;

        public DialogManager(string path, Transform parent, UiManager uiManager, CoroutineHelper coroutineHelper) : base(path, parent, uiManager, coroutineHelper) {}

        public D OpenDialog<D>() where D : BaseDialog
        {
            var dialog = SpawnView<D>();
            _currentSortOrder += SortOrderStep;
            dialog.Canvas.sortingOrder = _currentSortOrder;
            history.AddItem(dialog);
            dialog.Show();
            var dialogAnimator = dialog.GetComponent<UiAnimationPlayer>();
            if(dialogAnimator != null) dialogAnimator.PlayShowAnimation();
            SpawnOverlayDialog();
            return dialog;
        }

        public D OpenContextDataDialog<D, M>(M model) where D : BaseDialog, IContextDataOwner<M>
        {
            var dialog = OpenDialog<D>();
            dialog.ContextDataOwner.Initialize(model);
            return dialog;
        }

        public void Close(BaseDialog dialog)
        {
            if (dialog == null) return;

            history.RemoveItem(dialog);
            _currentSortOrder -= SortOrderStep;

            var dialogAnimator = dialog.GetComponent<UiAnimationPlayer>();
            var hasDialogAnimator = dialogAnimator != null;

            if (hasDialogAnimator)
            {
                RefreshOverlay();
                dialogAnimator.PlayHideAnimation(() => Object.Destroy(dialog.gameObject));
            }
            else
            {
                DestroyDialog(dialog);
            }
        }
        
        public void Close()
        {
            var lastDialog = history.LastItem;
            Close(lastDialog);
        }

        private void DestroyDialog(BaseDialog dialog)
        {
            RefreshOverlay();
            Object.Destroy(dialog.gameObject);
        }

        private void SpawnOverlayDialog()
        {
            if (_overlayDialog == null)
            {
                _overlayDialog = SpawnView<FadeOverlayDialog>();
                _overlayDialog.Canvas.sortingLayerID = UiSortingLayerId;
                _overlayDialog.Show();
                _overlayDialog.OnClickedEvent += OnOverlayClicked;
            }
            
            RefreshOverlay();
        }

        private void OnOverlayClicked()
        {
            if (history.Length == 0) return;

            if (history.LastItem.CloseOnOverlayClick)
            {
                history.LastItem.Close();
            }
        }

        private void RefreshOverlay()
        {
            var hasOpenedDialogs = history.Length > 0;

            if (hasOpenedDialogs)
            {
                var lastOpenedDialog = history.LastItem;
                _overlayDialog.Canvas.sortingOrder = lastOpenedDialog.Canvas.sortingOrder - 1;
            }
            else
            {
                DestroyOverlay();
            }
        }

        private void DestroyOverlay()
        {
            if(_overlayDialog != null)
            {
                _overlayDialog.OnClickedEvent -= OnOverlayClicked;
                Object.Destroy(_overlayDialog.gameObject);
            }
        }

        public override void CloseAll()
        {
            for (int i = history.Length - 1; i >= 0; i--)
            {
                DestroyDialog(history[i]);
                _currentSortOrder -= SortOrderStep;
            }
            
            History.Clear();
            DestroyOverlay();
        }
    }
}