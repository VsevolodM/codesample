﻿using System.IO;
using Common;
using Core.Interfaces;
using Extensions;
using Helpers;
using UI.Interfaces;
using UI.Views.Abstract;
using UnityEngine;

namespace UI.Managers.Abstract
{
    public abstract class BaseUiManager<T> where T : BaseView
    {
        protected readonly History<T> history;
        public History<T> History => history;

        private readonly Transform _uiParent;
        private readonly string _pathFromRoot;
        protected readonly int UiSortingLayerId;
        protected readonly UiManager UIManager;
        protected readonly CoroutineHelper CoroutineHelper;

        protected BaseUiManager(string folderName, Transform parent, UiManager uiManager, CoroutineHelper coroutineHelper)
        {
            history = new History<T>();
            UiSortingLayerId = SortingLayer.NameToID("UI");
            _pathFromRoot = Path.Combine(Constants.PrefabsRootFolder, folderName);
            _uiParent = new GameObject(folderName).transform;
            _uiParent.SetParent(parent);

            UIManager = uiManager;
            CoroutineHelper = coroutineHelper;
        }

        protected V SpawnView<V>() where V : BaseView
        {
            var path = Path.Combine(_pathFromRoot, typeof(V).Name);
            var view = UnityObjectExtensions.InstantiateFromResources<V>(path, _uiParent);

            if (view is ICanvasHolder canvasHolder)
            {
                canvasHolder.Canvas.renderMode = RenderMode.ScreenSpaceCamera;
                canvasHolder.Canvas.worldCamera = Camera.main;
            }

            view.Hide();
            return view;
        }

        protected V SpawnContextDataView<V, M>(M model) where V : BaseView, IContextDataOwner<M>
        {
            var prefab = SpawnView<V>();
            prefab.ContextDataOwner.Initialize(model);
            return prefab;
        }

        public V GetViewOfType<V>() where V : T
        {
            for (int i = 0; i < history.Items.Count; i++)
            {
                if (history[i] is V)
                {
                    return (V)history[i];
                }
            }

            return null;
        }
        
        public bool ManageEscapeClick()
        {
            if (history.Length == 0) return false;

            var lastItem = history.LastItem;

            if (lastItem.CloseOnEscape)
            {
                lastItem.OnEscapeKeyUp();
            }

            return lastItem.CloseOnEscape;
        }

        public abstract void CloseAll();
    }
}
