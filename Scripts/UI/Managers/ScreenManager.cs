﻿using Core.Interfaces;
using System;
using Helpers;
using UI.Managers.Abstract;
using UI.Screens.Abstract;
using UI.ScreenTransitions;
using UI.ScreenTransitions.Abstract;
using UnityEngine;
using Object = UnityEngine.Object;

namespace UI.Managers
{
    public class ScreenManager : BaseUiManager<BaseScreen>
    {
        private int _currentSortOrder = 500;
        private readonly BaseScreenTransition _screenTransition;
        
        public ScreenManager(string path, Transform parent, UiManager uiManager, CoroutineHelper coroutineHelper) : base(path, parent, uiManager, coroutineHelper)
        {
            _screenTransition = new FadeScreenTransition(UIManager, CoroutineHelper);
        }

        public S OpenScreen<S>(bool keepPreviousScreen) where S : BaseScreen
        {
            var screen = SpawnView<S>();
            history.AddItem(screen);
            screen.Canvas.worldCamera = Camera.main;
            screen.Canvas.sortingLayerID = UiSortingLayerId;
            PlayTransitionCoroutine(keepPreviousScreen, null, null);
            return screen;
        }

        public S OpenContextDataScreen<S, M>(bool keepPreviousScreen, M model) where S : BaseScreen, IContextDataOwner<M>
        {
            var screen = OpenScreen<S>(keepPreviousScreen);
            screen.ContextDataOwner.Initialize(model);
            return screen;
        }

        private void PlayTransitionCoroutine(bool keepPreviousScreen, Action onShown, Action onHidden)
        {
            var currentScreen = history.LastItem;
            currentScreen.Canvas.sortingOrder = ++_currentSortOrder;
            currentScreen.Show();

            _screenTransition.PlayShowAndHide(null, OnShown);

            void OnShown()
            {
                onShown?.Invoke();
                currentScreen.OnTransitionCompleted();
                
                if(history.Length > 1)
                {
                    var previousScreen = history.Items[history.Length - 2];

                    if (keepPreviousScreen)
                    {
                        previousScreen.Hide();
                    }
                    else
                    {
                        history.RemoveItem(previousScreen);
                        Object.Destroy(previousScreen.gameObject);
                    }
                    
                    onHidden?.Invoke();
                }
            }
        }

        public void Back()
        {
            var lastScreen = history.LastItem;

            if (lastScreen == null) return;

            history.RemoveItem(lastScreen);
            var previousScreen = history.LastItem;
            previousScreen?.Show();
            _currentSortOrder--;

            void OnHidden()
            {
                previousScreen.OnTransitionCompleted();
                DestroyScreen(lastScreen);
            }

            _screenTransition.PlayHide(OnHidden);
        }

        private void DestroyScreen(BaseScreen screen)
        {
            screen.Hide();
            Object.Destroy(screen.gameObject);
        }

        public override void CloseAll()
        {
            for (int i = history.Length - 1; i >= 0; i--)
            {
                DestroyScreen(history[i]);
                _currentSortOrder--;
            }
            
            History.Clear();
        }
    }
}
