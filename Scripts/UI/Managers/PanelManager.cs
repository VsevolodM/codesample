﻿using Core.Interfaces;
using Extensions;
using Helpers;
using UI.Managers.Abstract;
using UI.Panels.Abstract;
using UnityEngine;

namespace UI.Managers
{
    public class PanelManager : BaseUiManager<BasePanel>
    {
        public PanelManager(string path, Transform parent, UiManager uiManager, CoroutineHelper coroutineHelper) : base(path, parent, uiManager, coroutineHelper) {}

        public P OpenPanel<P>(Transform parent) where P : BasePanel
        {
            var panel = SpawnView<P>();
            panel.transform.SetParent(parent);
            panel.RectTransform.FillParent();
            history.AddItem(panel);
            panel.Show();
            return panel;
        }
        
        public P OpenContextDataPanel<P, M>(M model, Transform parent) where P : BasePanel, IContextDataOwner<M>
        {
            var panel = OpenPanel<P>(parent);
            panel.ContextDataOwner.Initialize(model);
            return panel;
        }

        public void Close(BasePanel panel)
        {
            history.RemoveItem(panel);
            panel.Hide();
            Object.Destroy(panel.gameObject);
        }

        public override void CloseAll()
        {
            for (int i = history.Length - 1; i >= 0; i--)
            {
                Close(history[i]);
            }
            
            History.Clear();
        }
    }
}
