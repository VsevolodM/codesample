﻿using Core.Types;
using Helpers;
using UnityEngine;
using Zenject;

namespace UI.Managers
{
    public class UiManager : CustomMonobehaviour
    {
        public ScreenManager ScreenManager { get; private set; }
        public DialogManager DialogManager { get; private set; }
        public PanelManager PanelManager { get; private set; }
        public Transform UiParent { get; private set; }
        
        private  CoroutineHelper _coroutineHelper;
        
        [Inject]
        private void Construct(CoroutineHelper coroutineHelper)
        {
            _coroutineHelper = coroutineHelper;
        }

        public void Awake()
        {
            UiParent = new GameObject("[UIRoot]").transform;
            ScreenManager = new ScreenManager("Screens", UiParent, this, _coroutineHelper);
            DialogManager = new DialogManager("Dialogs", UiParent, this, _coroutineHelper);
            PanelManager = new PanelManager("Panels", UiParent, this, _coroutineHelper);

            DontDestroyOnLoad(UiParent.gameObject);
        }
        
        private void OnEscapeKeyUp()
        {
            if (DialogManager.ManageEscapeClick()) { }
            else if (PanelManager.ManageEscapeClick()) { }
            else if (ScreenManager.ManageEscapeClick()) { }
        }
    }
}
