﻿using System.Collections.Generic;
using UnityEngine;

namespace UI.Managers
{
    public class History<T>
    {
        public List<T> Items { get; } = new List<T>();

        public T this[int index]
        {
            get
            {
                if (IsIndexInBounds(index))
                {
                    return Items[index];
                }

                ShowOutOfBoundsError(index);

                return default;
            }
        }

        public T LastItem
        {
            get
            {
                if (IsIndexInBounds(LastIndex))
                {
                    return Items[LastIndex];
                }

                return default;
            } 
        }

        public int LastIndex => Items.Count - 1;
        public int Length => Items.Count;

        public bool IsIndexInBounds(int index)
        {
            return index > -1 && index < Items.Count;
        }

        public void AddItem(T item)
        {
            Items.Add(item);
            OnItemAdded(item);
        }

        public void RemoveItem(T item)
        {
            var removed = Items.Remove(item);

            if (removed)
            {
                OnItemRemoved(item);
            }
        }

        public void RemoveItemAtIndex(int index)
        {
            if (IsIndexInBounds(index))
            {
                Items.Remove(this[index]);
                OnItemRemoved(this[index]);
            }
            else
            {
                ShowOutOfBoundsError(index);
            }
        }

        protected virtual void OnItemAdded(T item) {}
        protected virtual void OnItemRemoved(T item) {}

        public void Clear()
        {
            Items.Clear();
        }

        private void ShowOutOfBoundsError(int index)
        {
            Debug.LogError($"Index {index} is out of bounds of History list!");
        }
    }
}
