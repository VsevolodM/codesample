﻿using DG.Tweening;
using Extensions;
using System;
using UI.UIAnimators.Abstract;
using UnityEngine;

namespace UI.UIAnimators
{
    public class FadeUiAnimator : BaseGenericUiAnimator<CanvasGroup>
    {
        [SerializeField, Range(0f, 1f)] private float startAlpha;
        
        protected override Tween PlayHideAnimationInternal(Action callback = null)
        {
            return animationTarget.DOFade(0f, duration).SetEase(easeCurve).OnComplete(callback.SafeInvoke).Play();
        }

        protected override Tween PlayShowAnimationInternal(Action callback = null)
        {
            animationTarget.alpha = startAlpha;
            return animationTarget.DOFade(1f, duration).SetEase(easeCurve).OnComplete(callback.SafeInvoke).Play();
        }

        protected override void PlayHideAnimationInstantInternal()
        {
            animationTarget.alpha = 0f;
        }

        protected override void PlayShowAnimationInstantInternal()
        {
            animationTarget.alpha = 1f;
        }
    }
}
