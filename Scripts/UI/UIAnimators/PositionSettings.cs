﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace UI.UIAnimators
{
    [Serializable]
    public struct PositionSettings
    {
        [SerializeField] private PositionMode positionMode;
        [SerializeField, ShowIf(nameof(IsPositionModeManual))] private Vector2 anchoredPosition;
        [SerializeField, Range(-1f,1f), ShowIf(nameof(IsPositionModeScreenBased)), LabelText("X")] private float screenX;
        [SerializeField, Range(-1f,1f), ShowIf(nameof(IsPositionModeScreenBased)), LabelText("Y")] private float screenY;
        [SerializeField] private Vector2 anchorMin;
        [SerializeField] private Vector2 anchorMax;
        [SerializeField] private Vector2 pivot;

        public Vector2 AnchoredPosition
        {
            get
            {
                if (positionMode == PositionMode.Manual)
                {
                    return anchoredPosition;
                }
                else
                {
                    return new Vector2(Screen.width * screenX, Screen.height * screenY);
                }
            }
        }
        
        public Vector2 AnchorMin => anchorMin;
        public Vector2 AnchorMax => anchorMax;
        public Vector2 Pivot => pivot;

        private bool IsPositionModeManual => positionMode == PositionMode.Manual;
        private bool IsPositionModeScreenBased => positionMode == PositionMode.ScreenBased;

        public PositionSettings(Vector2 anchoredPosition, Vector2 anchorMin, Vector2 anchorMax, Vector2 pivot)
        {
            this.anchoredPosition = anchoredPosition;
            this.anchorMin = anchorMin;
            this.anchorMax = anchorMax;
            this.pivot = pivot;
            positionMode = PositionMode.Manual;
            screenX = 0f;
            screenY = 0f;
        }
    }

    public enum PositionMode
    {
        Manual, ScreenBased
    }
}
