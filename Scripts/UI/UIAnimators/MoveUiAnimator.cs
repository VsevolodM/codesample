﻿using DG.Tweening;
using Extensions;
using System;
using Sirenix.OdinInspector;
using UI.UIAnimators.Abstract;
using UnityEngine;

namespace UI.UIAnimators
{
    public class MoveUiAnimator : BaseGenericUiAnimator<RectTransform>
    {
        [SerializeField] private bool _keepCurrentPositionOnPlay;
        [SerializeField] private PositionSettings _appearPosition;
        [SerializeField] private PositionSettings _idlePosition;
        [SerializeField] private PositionSettings _disappearPosition;
        
        private Sequence _moveSequence;

        protected override Tween PlayShowAnimationInternal(Action callback = null)
        {
            if (!_keepCurrentPositionOnPlay)
            {
                animationTarget.SetPivotAndAnchors(_appearPosition.Pivot, _appearPosition.AnchorMin, _appearPosition.AnchorMax);
                animationTarget.anchoredPosition = _appearPosition.AnchoredPosition;
            }

            return PlayAnimation(_idlePosition, callback);
        }

        protected override Tween PlayHideAnimationInternal(Action callback = null)
        {
            return PlayAnimation(_disappearPosition, callback);
        }

        protected override void PlayHideAnimationInstantInternal()
        {
            PlayAnimationInstant(_disappearPosition);
        }

        private Tween PlayAnimation(PositionSettings targetSettings, Action callback = null)
        {
            _moveSequence?.Kill();
            _moveSequence.Restart();
            _moveSequence = DOTween.Sequence();
            _moveSequence.Join(animationTarget.DOPivot(targetSettings.Pivot, duration));
            _moveSequence.Join(animationTarget.DOAnchorMin(targetSettings.AnchorMin, duration));
            _moveSequence.Join(animationTarget.DOAnchorMax(targetSettings.AnchorMax, duration));
            _moveSequence.Join(animationTarget.DOAnchorPos(targetSettings.AnchoredPosition, duration));
            return _moveSequence.SetEase(easeCurve).Play().OnComplete(callback.SafeInvoke);
        }

        protected override void PlayShowAnimationInstantInternal()
        {
            PlayAnimationInstant(_idlePosition);
        }

        private void PlayAnimationInstant(PositionSettings targetSettings)
        {
            animationTarget.SetPivotAndAnchors(targetSettings.Pivot, targetSettings.AnchorMin, targetSettings.AnchorMax);
            animationTarget.anchoredPosition = targetSettings.AnchoredPosition;
        }

        [Button]
        private void SetCurrentDataForAppear()
        {
            _appearPosition = new PositionSettings(animationTarget.anchoredPosition, animationTarget.anchorMin, animationTarget.anchorMax, animationTarget.pivot);
        }

        [Button]
        private void SetCurrentDataForIdle()
        {
            _idlePosition = new PositionSettings(animationTarget.anchoredPosition, animationTarget.anchorMin, animationTarget.anchorMax, animationTarget.pivot);
        }

        [Button]
        private void SetCurrentDataForDisappear()
        {
            _disappearPosition = new PositionSettings(animationTarget.anchoredPosition, animationTarget.anchorMin, animationTarget.anchorMax, animationTarget.pivot);
        }

        public override void KillTween()
        {
            base.KillTween();
            _moveSequence?.Kill();
        }
    }
}