﻿using UnityEngine;

namespace UI.UIAnimators.Abstract
{
    public abstract class BaseGenericUiAnimator<T> : BaseUiAnimator where T : Component
    {
        [SerializeField] protected T animationTarget;
    }
}
