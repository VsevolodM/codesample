﻿using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

namespace UI.UIAnimators.Abstract
{
    [InlineEditor(Expanded = true)]
    public abstract class BaseUiAnimator
    {
        [SerializeField] protected float duration = 0.7f;
        [SerializeField] protected AnimationCurve easeCurve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

        public float Duration => duration;

        private Tween _tween;

        [Button]
        public void PlayShowAnimation(Action callback = null)
        {
            KillTween();
            _tween = PlayShowAnimationInternal(callback);
        }

        protected abstract Tween PlayShowAnimationInternal(Action callback = null);

        [Button]
        public void PlayHideAnimation(Action callback = null)
        {
            KillTween();
            _tween = PlayHideAnimationInternal(callback);
        }

        protected abstract Tween PlayHideAnimationInternal(Action callback = null);

        [Button]
        public void PlayShowAnimationInstant()
        {
            KillTween();
            PlayShowAnimationInstantInternal();
        }

        protected abstract void PlayShowAnimationInstantInternal();

        [Button]
        public void PlayHideAnimationInstant()
        {
            KillTween();
            PlayHideAnimationInstantInternal();
        }

        protected abstract void PlayHideAnimationInstantInternal();

        public virtual void KillTween()
        {
            _tween?.Kill();
        }
    }
}