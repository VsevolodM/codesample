﻿using Helpers;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using System;
using Core.Types;
using Extensions;
using UI.UIAnimators.Abstract;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

#pragma warning disable CS0649

namespace UI.UIAnimators
{
    public class UiAnimationPlayer : CustomSerializedMonobehaviour
    {
        [LabelText("Animation duration"), ShowInInspector, PropertyOrder(-1)]
        private float longestAnimationDuration
        {
            get
            {
                float maxDuration = 0f;

                if(animators != null)
                { 
                    for (int i = 0; i < animators.Length; i++)
                    {
                        if (animators[i] != null)
                        {
                            if (maxDuration < animators[i].Duration)
                            {
                                maxDuration = animators[i].Duration;
                            }
                        }
                    }
                }
                return maxDuration;
            }
        }

        [SerializeField, ListDrawerSettings(Expanded = true)] private BaseUiAnimator[] animators;
        private Coroutine _animationCallbackCoroutine;

        private CoroutineHelper _coroutineHelper;
        
        [Inject]
        protected virtual void Construct(CoroutineHelper coroutineHelper)
        {
            _coroutineHelper = coroutineHelper;
        }
        
        private void Awake()
        {
            Assert.AreNotEqual(animators.Length, 0);
        }

        public void PlayShowAnimation(Action callback = null)
        {
            CancelAllAnimations();
            animators.ForEach(animator => animator.PlayShowAnimation());
            _animationCallbackCoroutine = _coroutineHelper.ExecuteWithDelay(longestAnimationDuration, () => callback?.Invoke());
        }

        public void PlayHideAnimation(Action callback = null)
        {
            CancelAllAnimations();
            animators.ForEach(animator => animator.PlayHideAnimation());
            _animationCallbackCoroutine = _coroutineHelper.ExecuteWithDelay(longestAnimationDuration, () => callback?.Invoke());
        }

        private void CancelAllAnimations()
        {
            _coroutineHelper.SafeStopCoroutine(_animationCallbackCoroutine);
        }

        public void PlayShowAnimationInstant()
        {
            animators.ForEach(animator => animator.PlayShowAnimationInstant());
        }

        public void PlayHideAnimationInstant()
        {
            animators.ForEach(animator => animator.PlayHideAnimationInstant());
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            for (int i = 0; i < animators.Length; i++)
            {
                animators[i].KillTween();
            }
        }
    }
}
