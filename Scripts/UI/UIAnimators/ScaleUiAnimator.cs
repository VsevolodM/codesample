﻿using DG.Tweening;
using Extensions;
using System;
using UI.UIAnimators.Abstract;
using UnityEngine;

namespace UI.UIAnimators
{
    public class ScaleUiAnimator : BaseGenericUiAnimator<Transform>
    {
        [SerializeField] private bool resetScaleOnAnimationPlay = true;
        [SerializeField] private Vector2 startScale = new Vector2(0.2f, 0.2f);
        [SerializeField] private Vector2 endScale = Vector2.one;

        protected override Tween PlayShowAnimationInternal(Action callback = null)
        {
            if(resetScaleOnAnimationPlay)
            {
                animationTarget.localScale = startScale;
            }
            
            return animationTarget.DOScale(endScale, duration).SetEase(easeCurve).Play().OnComplete(callback.SafeInvoke);
        }

        protected override Tween PlayHideAnimationInternal(Action callback = null)
        {
            return animationTarget.DOScale(startScale, duration).SetEase(easeCurve).Play().OnComplete(callback.SafeInvoke);
        }

        protected override void PlayShowAnimationInstantInternal()
        {
            animationTarget.localScale = endScale;
        }

        protected override void PlayHideAnimationInstantInternal()
        {
            animationTarget.localScale = startScale;
        }
    }
}

