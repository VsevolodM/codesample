﻿using System;
using DG.Tweening;
using UnityEngine;

namespace UI.UIAnimators
{
    [Serializable]
    public class RotationSettings
    {
        [SerializeField] private Vector3 angle;
        [SerializeField] private int cyclesAmount = -1;
        [SerializeField] private RotateMode rotateMode = RotateMode.FastBeyond360;

        public Vector3 Angle => angle;
        public int CyclesAmount => cyclesAmount;
        public RotateMode RotateMode => rotateMode;

        public RotationSettings(Vector3 angle, int cyclesAmount, RotateMode rotateMode)
        {
            this.angle = angle;
            this.cyclesAmount = cyclesAmount;
            this.rotateMode = rotateMode;
        }
    }
}