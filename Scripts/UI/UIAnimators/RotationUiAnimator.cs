﻿using System;
using DG.Tweening;
using UI.UIAnimators.Abstract;
using UnityEngine;

namespace UI.UIAnimators
{
    public class RotationUiAnimator : BaseGenericUiAnimator<RectTransform>
    {
        [SerializeField] private RotationSettings settings;

        protected override Tween PlayShowAnimationInternal(Action callback = null)
        {
            return animationTarget.transform.DORotate(settings.Angle, duration, settings.RotateMode)
                .SetEase(easeCurve).SetLoops(settings.CyclesAmount).Play();
        }

        protected override Tween PlayHideAnimationInternal(Action callback = null)
        {
            return null;
        }

        protected override void PlayShowAnimationInstantInternal() {}

        protected override void PlayHideAnimationInstantInternal() {}
    }
}