﻿using UI.Dialogs.ConfirmationDialog;
using UI.Managers;
using UI.Screens.MainMenuScreen;

namespace UI.Services
{
    public class NavigationService
    {
        private readonly UiManager _uiManager;
        
        public NavigationService(UiManager uiManager)
        {
            _uiManager = uiManager;
        }
        
        #region Dialogs

        public ConfirmationDialog OpenConfirmationDialog(ConfirmationDialogModel dialogModel)
        {
            return _uiManager.DialogManager.OpenContextDataDialog<ConfirmationDialog, ConfirmationDialogModel>(dialogModel);
        }

        #endregion

        #region Screens
        
        public MainMenuScreen OpenMainMenuScreen()
        {
            return _uiManager.ScreenManager.OpenScreen<MainMenuScreen>(true);
        }
        
        #endregion

        #region Panels

        #endregion
    }
}