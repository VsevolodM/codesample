﻿using UI.Views.Abstract;

namespace UI.Panels.Abstract
{
    public abstract class BasePanel : BaseView
    {
        public override void Close()
        {
            base.Close();
            UIManager.PanelManager.Close(this);
        }
    }
}
