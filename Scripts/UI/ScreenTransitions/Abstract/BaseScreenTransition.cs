﻿using System;
using System.Collections;
using System.IO;
using Common;
using Extensions;
using Helpers;
using UI.Managers;
using UnityEngine;
using Object = UnityEngine.Object;

namespace UI.ScreenTransitions.Abstract
{
    public abstract class BaseScreenTransition
    {
        private const string TransitionsDirectoryName = "Transitions";

        protected float duration = 0.5f;

        protected abstract IEnumerator ShowCoroutine();
        protected abstract IEnumerator HideCoroutine();
        protected virtual void BeforePlayingTransition() {}

        private RectTransform _transitionsParent;
        private GameObject _transitionGameObject;
        private readonly UiManager _uiManager;
        private readonly CoroutineHelper _coroutineHelper;

        protected BaseScreenTransition(UiManager uiManager, CoroutineHelper coroutineHelper)
        {
            _uiManager = uiManager;
            _coroutineHelper = coroutineHelper;
        }

        private void SpawnTransitionParent()
        {
            var transitionParentName = "[TransitionsParent]";
            var parentGameObject = GameObject.Find(transitionParentName);

            if (parentGameObject != null)
            {
                _transitionsParent = GameObject.Find(transitionParentName).GetComponent<RectTransform>();
            }
            else
            {
                _transitionsParent = GameObjectExtensions.CreateNewGameObject(transitionParentName, _uiManager.UiParent, typeof(RectTransform)).GetComponent<RectTransform>();
                _transitionsParent.FillParent();
            }
        }

        protected T SpawnTransitionGameObject<T>() where T : Behaviour
        {
            if (_transitionsParent == null)
            {
                SpawnTransitionParent();
            }

            if(_transitionGameObject == null)
            {
                var prefabPath = Path.Combine(Constants.PrefabsRootFolder, TransitionsDirectoryName, GetType().Name);
                var spawnedTransition = UnityObjectExtensions.InstantiateFromResources<T>(prefabPath, _transitionsParent);
                _transitionGameObject = spawnedTransition.gameObject;
                SetLayer();
            }
            
            return _transitionGameObject.GetComponent<T>();
        }

        private void SetLayer()
        {
            var locationLayerId = SortingLayer.NameToID("UI");
            var canvas = _transitionGameObject.GetComponent<Canvas>();
            canvas.sortingLayerID = locationLayerId;
            canvas.sortingOrder = short.MaxValue;
        }

        private IEnumerator PlayShowAndHideCoroutine(Action onShown, Action onHidden)
        {
            BeforePlayingTransition();
            yield return ShowCoroutine();
            onShown?.Invoke();
            yield return HideCoroutine();
            onHidden?.Invoke();
            ManageTransitionGameObject();
        }
        
        private IEnumerator PlayShowCoroutine(Action onShown)
        {
            BeforePlayingTransition();
            yield return ShowCoroutine();
            onShown?.Invoke();
            ManageTransitionGameObject();
        }
        
        private IEnumerator PlayHideCoroutine(Action onHidden)
        {
            yield return HideCoroutine();
            onHidden?.Invoke();
            ManageTransitionGameObject();
        }

        public void PlayShow(Action onShown)
        {
            _coroutineHelper.StartCoroutine(PlayShowCoroutine(onShown));
        }
        
        public void PlayHide(Action onHidden)
        {
            _coroutineHelper.StartCoroutine(PlayHideCoroutine(onHidden));
        }

        public void PlayShowAndHide(Action onShown, Action onHidden)
        {
            _coroutineHelper.StartCoroutine(PlayShowAndHideCoroutine(onShown, onHidden));
        }

        protected virtual void ManageTransitionGameObject()
        {
            Object.Destroy(_transitionGameObject);
        }
    }
}
