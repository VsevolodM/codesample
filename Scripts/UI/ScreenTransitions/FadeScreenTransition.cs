﻿using DG.Tweening;
using System.Collections;
using Common;
using Helpers;
using UI.Managers;
using UI.ScreenTransitions.Abstract;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace UI.ScreenTransitions
{
    public class FadeScreenTransition : BaseScreenTransition
    {
        private Image _fadeImage;
        
        public FadeScreenTransition(UiManager uiManager, CoroutineHelper coroutineHelper) : base(uiManager, coroutineHelper) {}

        protected override IEnumerator ShowCoroutine()
        {
            _fadeImage.color = Constants.TransparentBlack;
            yield return _fadeImage.DOFade(1f, duration).Play().WaitForCompletion();
        }

        protected override IEnumerator HideCoroutine()
        {
            yield return _fadeImage.DOFade(0f, duration).Play().WaitForCompletion();
        }

        protected override void BeforePlayingTransition()
        {
            base.BeforePlayingTransition();

            if (_fadeImage == null)
            {
                _fadeImage = SpawnTransitionGameObject<Image>();
            }
        }

        protected override void ManageTransitionGameObject()
        {
            base.ManageTransitionGameObject();
            _fadeImage = null;
        }
    }
}
