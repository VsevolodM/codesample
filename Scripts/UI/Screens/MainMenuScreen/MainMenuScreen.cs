using Sirenix.OdinInspector;
using UI.Screens.Abstract;
using UI.Services;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Zenject;

namespace UI.Screens.MainMenuScreen
{
    public class MainMenuScreen : BaseScreen
    {
        [SerializeField, ChildGameObjectsOnly] private Button newGameButton;
        [SerializeField, ChildGameObjectsOnly] private Button settingsButton;
        [SerializeField, ChildGameObjectsOnly] private Button exitButton;

        private NavigationService _navigationService;
        
        [Inject]
        private void Construct(NavigationService navigationService)
        {
            _navigationService = navigationService;
        }
        
        protected override void Awake()
        {
            base.Awake();
            Assert.IsNotNull(newGameButton);
            Assert.IsNotNull(settingsButton);
            Assert.IsNotNull(exitButton);
        }

        private void OnEnable()
        {
            newGameButton.onClick.AddListener(OnNewGameButtonClicked);
            settingsButton.onClick.AddListener(OnSettingsButtonClicked);
            exitButton.onClick.AddListener(OnExitButtonClicked);
        }

        private void OnDisable()
        {
            newGameButton.onClick.RemoveListener(OnNewGameButtonClicked);
            settingsButton.onClick.RemoveListener(OnSettingsButtonClicked);
            exitButton.onClick.RemoveListener(OnExitButtonClicked);
        }

        private void OnNewGameButtonClicked()
        {
            _navigationService.OpenMainMenuScreen();
        }
        
        private void OnSettingsButtonClicked() {}
        
        private void OnExitButtonClicked()
        {
            Application.Quit();
        }
    }
}
