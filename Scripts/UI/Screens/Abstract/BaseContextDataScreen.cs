﻿using Core.Interfaces;
using Core.Types;

namespace UI.Screens.Abstract
{
    public abstract class BaseContextDataScreen<T> : BaseScreen, IContextDataOwner<T>
    {
        public ContextDataOwner<T> ContextDataOwner { get; private set; }

        protected override void Awake()
        {
            base.Awake();
            ContextDataOwner = new ContextDataOwner<T>(OnInitialized, OnPreUninitialized, OnDataPreUninitialized);
        }

        protected abstract void OnInitialized();
        protected virtual void OnPreUninitialized() {}
        protected virtual void OnDataPreUninitialized() {}
    }
}
