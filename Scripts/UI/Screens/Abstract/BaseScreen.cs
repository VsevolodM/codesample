﻿using UI.Interfaces;
using UI.Views.Abstract;
using UnityEngine;

namespace UI.Screens.Abstract
{
    public abstract class BaseScreen : BaseView, ICanvasHolder
    {
        public Canvas Canvas { get; private set; }

        protected override void Awake()
        {
            base.Awake();
            Canvas = GetComponent<Canvas>();
        }

        public virtual void OnTransitionCompleted() {}

        public override void Close()
        {
            base.Close();
            UIManager.ScreenManager.Back();
        }
    }
}
