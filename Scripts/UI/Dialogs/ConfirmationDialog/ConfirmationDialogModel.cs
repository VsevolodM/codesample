using System;

namespace UI.Dialogs.ConfirmationDialog
{
    public class ConfirmationDialogModel
    {
        public string MessageLocalizationKey { get; private set; }
        public string YesTextLocalizationKey { get; private set; }
        public string NoTextLocalizationText { get; private set; }
        public Action YesButtonAction { get; private set; }
        public Action NoButtonAction { get; private set; }

        public ConfirmationDialogModel(string messageLocalizationKey, string yesTextLocalizationKey, string noTextLocalizationText, Action yesButtonAction, Action noButtonAction = null)
        {
            MessageLocalizationKey = messageLocalizationKey;
            YesTextLocalizationKey = yesTextLocalizationKey;
            NoTextLocalizationText = noTextLocalizationText;
            YesButtonAction = yesButtonAction;
            NoButtonAction = noButtonAction;
        }
    }
}