using Localization;
using TMPro;
using UI.Dialogs.Abstract;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace UI.Dialogs.ConfirmationDialog
{
    public class ConfirmationDialog : BaseContextDataDialog<ConfirmationDialogModel>
    {
        [SerializeField] private TextMeshProUGUI messageText;
        [SerializeField] private TextMeshProUGUI noButtonText;
        [SerializeField] private TextMeshProUGUI yesButtonText;
        [SerializeField] private Button noButton;
        [SerializeField] private Button yesButton;

        private bool _closingWithConfirmation;
        
        protected override void Awake()
        {
            base.Awake();
            
            Assert.IsNotNull(messageText);
            Assert.IsNotNull(noButtonText);
            Assert.IsNotNull(yesButtonText);
            Assert.IsNotNull(noButton);
            Assert.IsNotNull(yesButton);

            yesButton.onClick.AddListener(OnYesButtonClicked);
            noButton.onClick.AddListener(OnNoButtonClicked);
        }
        
        protected override void OnInitialized()
        {
            messageText.text = LocalizationProvider.GetString(ContextDataOwner.ContextData.MessageLocalizationKey);
            noButtonText.text = LocalizationProvider.GetString(ContextDataOwner.ContextData.NoTextLocalizationText);
            yesButtonText.text = LocalizationProvider.GetString(ContextDataOwner.ContextData.YesTextLocalizationKey);
        }

        private void OnYesButtonClicked()
        {
            _closingWithConfirmation = true;
            Close();
        }

        private void OnNoButtonClicked()
        {
            Close();
        }

        public override void Close()
        {
            RemoveButtonListeners();
            
            if (_closingWithConfirmation)
            {
                ContextDataOwner.ContextData.YesButtonAction?.Invoke();
            }
            else
            {
                ContextDataOwner.ContextData.NoButtonAction?.Invoke();
            }
            
            base.Close();
        }

        private void RemoveButtonListeners()
        {
            yesButton.onClick.RemoveListener(OnYesButtonClicked);
            noButton.onClick.RemoveListener(OnNoButtonClicked);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            RemoveButtonListeners();
        }
    }
}