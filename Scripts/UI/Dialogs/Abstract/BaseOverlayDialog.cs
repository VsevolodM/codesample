﻿using System;
using UnityEngine.UI;

namespace UI.Dialogs.Abstract
{
    public abstract class BaseOverlayDialog : BaseDialog
    {
        public event Action OnClickEvent;

        protected bool IsEffectEnabled { get; set; }
        private Button _button;

        protected override void Awake()
        {
            base.Awake();
            _button = Body.GetComponent<Button>();
        }

        protected override void OnShown()
        {
            base.OnShown();
            _button.onClick.AddListener(OnClicked);
        }

        protected override void OnHidden()
        {
            base.OnHidden();
            _button.onClick.RemoveListener(OnClicked);
        }

        private void OnClicked()
        {
            OnClickEvent?.Invoke();
        }

        public abstract void EnableEffect(bool enable, bool overtime, Action callback = null);
    }
}
