﻿using System;
using UI.Interfaces;
using UI.Managers;
using UI.Views.Abstract;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

namespace UI.Dialogs.Abstract
{
    public abstract class BaseDialog : BaseView, ICanvasHolder
    {
        public event Action OnClosedEvent;
        
        [FormerlySerializedAs("_closeOnOverlayClick")] [SerializeField] private bool closeOnOverlayClick = true;

        private Canvas _canvas;
        public Canvas Canvas
        {
            get
            {
                if (_canvas == null)
                {
                    _canvas = GetComponent<Canvas>();
                }

                return _canvas;
            }
        }

        public bool CloseOnOverlayClick => closeOnOverlayClick;
        
        public override void Close()
        {
            base.Close();
            OnClosedEvent?.Invoke();
            UIManager.DialogManager.Close();
        }
    }
}
