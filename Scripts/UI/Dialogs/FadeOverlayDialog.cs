﻿using System;
using Sirenix.OdinInspector;
using UI.Dialogs.Abstract;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace UI.Dialogs
{
    public class FadeOverlayDialog : BaseDialog
    {
        public event Action OnClickedEvent;

        [SerializeField, ChildGameObjectsOnly] private Button overlayButton;

        protected override void Awake()
        {
            base.Awake();
            Assert.IsNotNull(overlayButton);
        }

        private void OnEnable()
        {
            overlayButton.onClick.AddListener(OnButtonClicked);
        }

        private void OnDisable()
        {
            overlayButton.onClick.RemoveListener(OnButtonClicked);
        }

        private void OnButtonClicked()
        {
            OnClickedEvent?.Invoke();
        }
    }
}
